# -*- mode: makefile; coding: utf-8 -*-
# Copyright © 2006 Sylvain Le Gall <gildor#debian.org>
# Copyright © 2007 Carl Fürstenberg <azatoth@gmail.com>
# Description: Rules to manage manpages written in XML.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
# MA 02110-1301, USA. 

#PATH_RULES#

ifndef _cdbs_rules_docbook_xml_manpage
	_cdbs_rules_docbook_xml_manpage = 1

include $(_cdbs_rules_path)/buildcore.mk$(_cdbs_makefile_suffix)

# docbook-xml manpages to build
DOCBOOKXML_MANPAGE_SOURCES := 

# add required build dependency for used tools 
CDBS_BUILD_DEPENDS := $(CDBS_BUILD_DEPENDS), docbook-xml, docbook-xsl, libxml2-utils, xsltproc, libxml-libxml-perl

# xmllint
DOCBOOKXML_MANPAGE_XMLLINT = xmllint --nonet --nocdata --noent --noout --postvalid --xinclude $(1)

# if make should abort if xmllint reports anything
DOCBOOKXML_MANPAGE_XMLLINT_ABORT_ON_ERROR = yes

# xsltproc
DOCBOOKXML_MANPAGE_XSLTPROC = xsltproc --nonet --xinclude --param man.charmap.use.subset 0 -o $(dir $(1)) $(2) $(1)

# XSL stylesheet to transform docbook-xml to manpage
DOCBOOKXML_MANPAGE_XSLTPROC_XSL = /usr/share/xml/docbook/stylesheet/nwalsh/manpages/docbook.xsl

#
# This definition handles all the processing, it will abort if xmllint prints anything
# (thus must be fully clean) unless DOCBOOKXML_MANPAGE_XMLLINT_ABORT_ON_ERROR is not "yes".
# It will parse the xml files, to find what files that will be created, to be able to remove them
# on clean.
#
define DOCBOOKXML_MANPAGE_PROCESS
	echo $(call DOCBOOKXML_MANPAGE_XMLLINT, $(1)); \
	res=`$(call DOCBOOKXML_MANPAGE_XMLLINT, $(1))`; \
	if [ "x$(DOCBOOKXML_MANPAGE_XMLLINT_ABORT_ON_ERROR)" = "xyes" -a "$$res" ]; then \
		echo $$res; \
		exit 2; \
	fi; \
	perl -MXML::LibXML -e ' \
	foreach my $$context( (new XML::LibXML())->parse_file("$(1)")->findnodes("//refentry")) { \
		$$name = lc($$context->findvalue(".//refmeta/refentrytitle")); \
		$$section = $$context->findvalue(".//refmeta/manvolnum"); \
		push @manpages, "$$name.$$section"; \
	} \
	print "generating ", join(" ", map {"$(dir $(1))".$$_} @manpages), "\n"; \
	open LIST, ">> debian/docbook-xml-manpage-list"; \
	print LIST join("\n", map {"$(dir $(1))".$$_} @manpages), "\n"; \
	close LIST;'; \
	echo $(call DOCBOOKXML_MANPAGE_XSLTPROC, $(1), $(DOCBOOKXML_MANPAGE_XSLTPROC_XSL)); \
	$(call DOCBOOKXML_MANPAGE_XSLTPROC, $(1), $(DOCBOOKXML_MANPAGE_XSLTPROC_XSL));

endef

clean::
	@if test -f debian/docbook-xml-manpage-list; then \
		for i in `cat debian/docbook-xml-manpage-list`; do \
		$(RM) $$i;echo $(RM) $$i; \
		done; \
		fi
	-$(RM) debian/docbook-xml-manpage-list;
	-$(RM) debian/docbook-xml-manpage-stamp;

post-patches:: debian/docbook-xml-manpage-stamp
debian/docbook-xml-manpage-stamp: 
	@$(foreach source,$(DOCBOOKXML_MANPAGE_SOURCES),$(call DOCBOOKXML_MANPAGE_PROCESS,$(source)))
	touch $@

endif
